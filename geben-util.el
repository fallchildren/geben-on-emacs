(require 'cl)
(require 'gdb-mi)

;;==============================================================
;; utilities
;;==============================================================

(defsubst geben-flatten (x)
  "Make cons X to a flat list."
  (flet ((rec (x acc)
              (cond ((null x) acc)
                    ((atom x) (cons x acc))
                    (t (rec (car x) (rec (cdr x) acc))))))
    (rec x nil)))

(defsubst geben-what-line (&optional pos)
  "Get the number of the line in which POS is located.
If POS is omitted, then the current position is used."
  (save-restriction
    (widen)
    (save-excursion
      (if pos (goto-char pos))
      (beginning-of-line)
      (1+ (count-lines 1 (point))))))

(defmacro geben-plist-push (plist prop value)
  `(let* ((plist ,plist)
          (l (plist-get plist ,prop)))
     (cond
      ((consp l)
       (plist-put plist ,prop
                  (cons ,value (plist-get plist ,prop))))
      ((null l)
       (plist-put plist ,prop (list ,value)))
      (t
       (error "geben-plist-push: cannot add value; type of prop `%s' is not `list' but `%s'."
              ,prop (type-of ,value))))))

(defmacro geben-plist-append (plist prop value)
  `(let* ((plist ,plist)
          (l (plist-get plist ,prop)))
     (cond
      ((consp l)
       (nconc l (list ,value)))
      ((null l)
       (plist-put plist ,prop (list ,value)))
      (t
       (error "geben-plist-add: cannot add value; type of prop `%s' is not `list' but `%s'."
              ,prop (type-of ,value))))))

(defmacro geben-lexical-bind (bindings &rest body)
  (declare (indent 1)
           (debug (sexp &rest form)))
  (cl-macroexpand-all
   (nconc
    (list 'lexical-let (mapcar (lambda (arg)
                                 (list arg arg))
                               bindings))
    body)))

(defun geben-remove-directory-tree (basedir)
  (ignore-errors
    (mapc (lambda (path)
            (cond
             ((or (file-symlink-p path)
                  (file-regular-p path))
              (delete-file path))
             ((file-directory-p path)
              (let ((name (file-name-nondirectory path)))
                (or (equal "." name)
                    (equal ".." name)
                    (geben-remove-directory-tree path))))))
          (directory-files basedir t nil t))
    (delete-directory basedir)))

(defun geben-remote-p (ip)
  "Test whether IP refers a remote system."
  (not (or (equal ip "127.0.0.1")
           (and (fboundp 'network-interface-list)
                (member ip (mapcar (lambda (addr)
                                     (format-network-address (cdr addr) t))
                                   (network-interface-list)))))))

;;--------------------------------------------------------------
;;  cross emacs overlay definitions
;;--------------------------------------------------------------

(eval-and-compile
  (and (featurep 'xemacs)
       (require 'overlay))
  (or (fboundp 'overlay-livep)
      (defalias 'overlay-livep 'overlay-buffer)))

(defun geben-overlay-make-line (lineno &optional buf)
  "Create a whole line overlay."
  (when (bufferp buf)
    (with-current-buffer buf
      (save-excursion
        (geben-put-breakpoint-icon t lineno)))))

;; modified gdb-put-breakpoint-icon from emacs gdb-mi.el
(defun geben-put-breakpoint-icon (enabled line)
  (let* ((posns (gdb-line-posns (or line (line-number-at-pos))))
         (start (- (car posns) 1))
         (end (+ (cdr posns) 1))
         (putstring (if enabled "B" "b"))
         (source-window (get-buffer-window (current-buffer) 0)))
    ;; (add-text-properties
    ;;  0 1 '(help-echo "mouse-1: clear bkpt, mouse-3: enable/disable bkpt")
    ;;  putstring)
    (gdb-remove-breakpoint-icons start end)
    (if (display-images-p)
        (if (>= (or left-fringe-width
                    (if source-window (car (window-fringes source-window)))
                    geben-buffer-fringe-width) 8)
            (gdb-put-string
             nil (1+ start)
             `(left-fringe breakpoint
                           ,(if enabled
                                'breakpoint-enabled
                              'breakpoint-disabled))
             'gdb-enabled enabled)
          (when (< left-margin-width 2)
              (setq left-margin-width 2)
            (save-current-buffer
              (if source-window
                  (set-window-margins
                   source-window
                   left-margin-width right-margin-width))))
          (put-image
           (if enabled
               (or breakpoint-enabled-icon
                   (setq breakpoint-enabled-icon
                         (find-image `((:type xpm :data
                                        ,breakpoint-xpm-data
                                        :ascent 100 :pointer hand)
                                       (:type pbm :data
                                        ,breakpoint-enabled-pbm-data
                                        :ascent 100 :pointer hand)))))
             (or breakpoint-disabled-icon
                 (setq breakpoint-disabled-icon
                       (find-image `((:type xpm :data
                                      ,breakpoint-xpm-data
                                      :conversion disabled
                                      :ascent 100 :pointer hand)
                                     (:type pbm :data
                                      ,breakpoint-disabled-pbm-data
                                      :ascent 100 :pointer hand))))))
           (+ start 1)
           putstring
           'left-margin))
      (when (< left-margin-width 2)
        (save-current-buffer
          (setq left-margin-width 2)
          (let ((window (get-buffer-window (current-buffer) 0)))
            (if window
                (set-window-margins
                 window left-margin-width right-margin-width)))))
      (gdb-put-string
       (propertize putstring
                   'face (if enabled
                             'breakpoint-enabled 'breakpoint-disabled))
       (1+ start)))))


(provide 'geben-util)
